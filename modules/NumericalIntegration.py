from __future__ import print_function
from __future__ import division

from builtins import object
import numpy as np
from scipy import integrate

from polylib import euler


# TODO: Choose more sensible names...
class Integrate(object):
    def __init__(self, molecule1, molecule2, m, betaN, hbar=1.):
        self.m = np.reshape(m, (-1, 1))
        self.molN = np.sqrt(self.m)*molecule1 # xN
        self.mol1 = np.sqrt(self.m)*molecule2 # x1
        self.betaN = betaN
        self.hbar = hbar
        self.omegan = 1/self.betaN/self.hbar
        self.prefactor = np.square(self.omegan)*self.betaN/2
        self.eu = euler.Euler()

    def f(self, psi, theta, phi):
        rot = self.eu.matrix(phi, theta, psi)
        mol1_rot = np.dot(self.mol1, np.transpose(rot))
        return np.exp(-self.prefactor*np.sum((self.molN - mol1_rot)**2))*np.sin(theta)

    def integrateEulerAngles(self):
        integral, accuracy = integrate.tplquad(self.f, 0, 2*np.pi, 0, np.pi, 0, 2*np.pi)
        return integral
