from __future__ import print_function
from __future__ import division

import numpy as np
import matplotlib.pyplot as plt

from polylib import elements
from polylib import units
from polylib import vmd
from polylib import rotational

import modules.fN as fN

import datetime

# TODO: Choose more sensible names...
# TODO: Divide functions in better testable parts and maybe test them.


# Simulation conditions:
UNITS = units.hartAng()
beta = UNITS.betaTemp(200) # convert 200 K to beta
numBeads = 48
hbar = 1.
betaN = beta/numBeads
omegan = 1/(betaN*hbar)


if __name__ == "__main__":
    Traj0, atomlist = vmd.load("hydroniumTrajReplica0.xyz", getatomlist=True) # xN
    Traj1 = vmd.load("hydroniumTrajReplica1.xyz") # x1
    m = elements.getmass(atomlist) * units.hartAng().amu  # shape = (4, 1)
    numStructures = Traj0.shape[0]  # number of structures to integrate, max = Traj0.shape[0]

    fNs = np.empty(numStructures)
    for i in range(numStructures):
        fNobj = fN.fN(Traj0[i], Traj1[i], m, betaN, hbar)
        fNs[i] = fNobj.calculatefN()
    np.savetxt("fN.csv", fNs)
    print(np.mean(fNs)) # 3.145141425601679e-05

    xaxis = np.arange(Traj0.shape[0])*10
    fig, ax = plt.subplots()
    ax.plot(xaxis[1:1000], fNs[1:1000])
    ax.set(xlabel='time (fs)', ylabel='f_N(xN, x1)',
       title='')
    fig.savefig("fns.png")
    plt.show()

    rotZ = np.zeros(Traj0.shape[0])
    for i in range(Traj0.shape[0]):
        ABC = np.array(rotational.constants(Traj0[i], m, units.hartAng()))
        oblate = rotational.asymmetric(ABC[0], ABC[1], ABC[2], maxJ=3, eigvals_only=True, convention='IIIr')
        rotZ[i] = rotational.qrotnonlinear(ABC[0], ABC[1], ABC[2], beta)
        # rotZ[i] = rotational.qrotexact(oblate, beta) # Not working...
    rotZ = np.mean(rotZ)
    print(1/rotZ) # 0.008869255192672062

    
    print("Manually:")
    r2mHydrogen = np.square(0.94)*m[0]
    r2mOx = np.square(0.28)*m[3]
    C = hbar**2/(2*r2mHydrogen*3)
    B = hbar**2/(r2mHydrogen*3+2*r2mOx)
    oblate = rotational.oblate(B, C)
    rotZ = rotational.qrotexact(oblate, beta)
    print(1/rotZ) # 0.005621875336433583
