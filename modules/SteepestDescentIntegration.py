from __future__ import print_function
from __future__ import division

from builtins import object
import numpy as np

from polylib import eckart_frame

class SteepestDescent(object):
    def __init__(self, molecule1, molecule2, masses, betaN, hbar=1.):
        self.m = np.reshape(masses, (-1, 1))
        self.xN = np.sqrt(self.m)*molecule1
        self.x1 = np.sqrt(self.m)*molecule2
        self.betaN = betaN
        self.hbar = hbar
        self.omegan = 1/betaN/hbar
        self.prefactor = (2*np.pi/betaN/(self.omegan**2))**1.5
        self.eck = eckart_frame.EckartFrame(molecule1, self.m.flatten())
        self.x1tilde, self.rotmatrix, self.norm2 = self.eck.toEckart(molecule2)
        self.x1tilde *= np.sqrt(self.m) # mass-weighting
        self.det = self.setDeterminant()
        self.expTerm = np.exp(-betaN*self.omegan**2/2*self.norm2)

    def setDeterminant(self):
        prod = self.xN * self.x1tilde
        sumOverAtoms = np.sum(prod, axis=0)
        sumtot = np.sum(sumOverAtoms)
        A11 = sumtot - sumOverAtoms[0]
        A22 = sumtot - sumOverAtoms[1]
        A33 = sumtot - sumOverAtoms[2]
        A12 = -np.sum(self.xN[:,1]*self.x1tilde[:,0])
        A13 = -np.sum(self.xN[:,2]*self.x1tilde[:,0])
        A23 = -np.sum(self.xN[:,2]*self.x1tilde[:,1])
        A = np.array([[A11, A12, A13], [A12, A22, A23], [A13, A23, A33]])
        return np.linalg.det(A)

    def performIntegration(self):
        return self.prefactor/np.sqrt(self.det)*self.expTerm
