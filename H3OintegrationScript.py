from __future__ import print_function
from __future__ import division

import numpy as np
import matplotlib.pyplot as plt

from polylib import elements
from polylib import units
from polylib import vmd
from polylib import rotational

import modules.NumericalIntegration as NI
import modules.SteepestDescentIntegration as SDI

import datetime

# TODO: Choose more sensible names...
# TODO: Divide functions in better testable parts and maybe test them.


# Simulation conditions:
UNITS = units.hartAng()
beta = UNITS.betaTemp(200) # convert 200 K to beta
numBeads = 48
hbar = 1.
betaN = beta/numBeads
omegan = 1/(betaN*hbar)


if __name__ == "__main__":
    Traj0, atomlist = vmd.load("hydroniumTrajReplica0.xyz", getatomlist=True) # xN
    Traj1 = vmd.load("hydroniumTrajReplica1.xyz") # x1
    m = elements.getmass(atomlist) * units.hartAng().amu  # shape = (4, 1)
    numStructures = Traj0.shape[0]  # number of structures to integrate, max = Traj0.shape[0]

    # Integrate numerically:

    numInts = np.empty(numStructures)
    a = datetime.datetime.now()
    for i in range(numStructures):
        integ = NI.Integrate(Traj0[i], Traj1[i], m, betaN, hbar)
        numInts[i] = integ.integrateEulerAngles()
    print(datetime.datetime.now()-a)
    np.savetxt("numericalIntegrationValues.csv", numInts)


    # Steepest Descent Integration:
    integrals = np.empty(numStructures)
    for i in range(numStructures):
        sd = SDI.SteepestDescent(Traj0[i], Traj1[i], m, betaN, hbar)
        integrals[i] = sd.performIntegration()
    np.savetxt("SteepestDescentValues.csv", integrals)

